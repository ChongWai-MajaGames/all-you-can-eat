import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'gsap';
import { map, each, sum } from 'lodash';

import app from './core/app';
import state from './core/state';
import ui from './core/ui';
import sound from './core/sound';
import Slot from './core/slot';
import utils from './core/utils';

import apiHelper from './api-helper';
import panel from './core/panel';

const isMobile = app.isMobile();
let loader = PIXI.loader;
let loaded = loader.resources;

let bonusSplatSpine;
let characterAnimState = {
    idleAnimation: 0,
    winAnimation: 1,
};

/** -------------------------
 *  Images
 * ------------------------*/
// Background Images
let background;
let foreground;
let paytable;
let paytablePage = [];
let button_close;
let button_previous;
let button_next;
let plate1;
let plate2;
let plate3;
let plate4;
let table;
let winPanel;

let characterSpine;
let bonusSpine;
let plateSprite;

let totemHighlight;

let speed = 1;

/** -------------------------
 *  Dynamic Font textures
 *  Initialize into cache for reusable purpose
 * ------------------------*/
let sprite_bonus_title;
let sprite_bonus_background;
let spriteNumberTexture = {};

/** -------------------------
 *  Mobile
 * ------------------------*/

/** -------------------------
 *  Variables
 * ------------------------*/
let slots = [];                 // All the slot containing its texture and type info.
let winPanels = [];

// Stomach charge meter Instances
let bonusChargeLevel = 0;
let stomach_charge_base;
let stomach_charge_cover;
let stomach_charge_meter;

// Miscs
let zombieContainer;            // Drawing Container to store all the Zombie animation sprites
let betLine;

const patterns = [
    {'id': 1, 'path': 'b1, b2, b3, b4, b5'},
    {'id': 2, 'path': 'a1, a2, a3, a4, a5'},
    {'id': 3, 'path': 'c1, c2, c3, c4, c5'},
    {'id': 4, 'path': 'a1, b2, c3, b4, a5'},
    {'id': 5, 'path': 'c1, b2, a3, b4, c5'},
    {'id': 6, 'path': 'b1, a2, a3, a4, b5'},
    {'id': 7, 'path': 'b1, c2, c3, c4, b5'},
    {'id': 8, 'path': 'a1, a2, b3, c4, c5'},
    {'id': 9, 'path': 'c1, c2, b3, a4, a5'},
    {'id': 10, 'path': 'b1, c2, b3, a4, b5'},
    {'id': 11, 'path': 'b1, a2, b3, c4, b5'},
    {'id': 12, 'path': 'a1, b2, b3, b4, a5'},
    {'id': 13, 'path': 'c1, b2, b3, b4, c5'},
    {'id': 14, 'path': 'a1, b2, a3, b4, a5'},
    {'id': 15, 'path': 'c1, b2, c3, b4, c5'}
];

let betLinesContainer;
let uiPanel;
let particles = [];

let lineColourArray = [
    0x00ff06,
    0xff0000,
    0xffff00,
    0x0000ff,
    0x00fcff,
    0x00ff8a,
    0xff8a00,
    0xDC143C,
    0xC0C0C0,
    0x808080,
    0x800000,
    0x800080,
    0x008080,
    0x000080,
    0xE6E6FA,
    0xFFFAF0,
    0xF0F8FF,
    0xF8F8FF,
    0xFF1493,
    0xFF69B4,
];

function init() {

    state.set('betLine', 1);
    state.set('betAmount', 1);
    state.set('totalBet', 1);
    state.set('balance', 0);

    let totems = {
        'x': [
            loaded.totem_x.texture,
            loaded.texture_wildcard_1.texture,
            loaded.texture_wildcard_2.texture,
            loaded.texture_wildcard_3.texture,
            loaded.texture_wildcard_4.texture,
            loaded.texture_wildcard_5.texture,
            loaded.texture_wildcard_6.texture,
            loaded.texture_wildcard_7.texture,
            loaded.texture_wildcard_8.texture,
            loaded.texture_wildcard_9.texture,
            loaded.texture_wildcard_10.texture,
            loaded.texture_wildcard_11.texture,
            loaded.texture_wildcard_12.texture,
            loaded.texture_wildcard_13.texture,
            loaded.texture_wildcard_14.texture,
            loaded.texture_wildcard_15.texture,
            loaded.texture_wildcard_16.texture,
            loaded.texture_wildcard_17.texture,
            loaded.texture_wildcard_18.texture,
            loaded.texture_wildcard_19.texture,
        ],
        'a': [loaded.totem_a.texture],
        'b': [loaded.totem_b.texture],
        'c': [loaded.totem_c.texture],
        'd': [loaded.totem_d.texture],
        'e': [loaded.totem_e.texture],
        'f': [loaded.totem_f.texture],
        'g': [loaded.totem_g.texture],
        'h': [loaded.totem_h.texture],
        'i': [
            loaded.totem_y.texture,
            loaded.texture_scatter_1.texture,
            loaded.texture_scatter_2.texture,
            loaded.texture_scatter_3.texture,
            loaded.texture_scatter_4.texture,
            loaded.texture_scatter_5.texture,
            loaded.texture_scatter_6.texture,
            loaded.texture_scatter_7.texture,
            loaded.texture_scatter_8.texture,
            loaded.texture_scatter_9.texture,
            loaded.texture_scatter_10.texture,
            loaded.texture_scatter_11.texture,
            loaded.texture_scatter_12.texture,
            loaded.texture_scatter_13.texture,
            loaded.texture_scatter_14.texture,
        ],
    };

    totemHighlight = [
        loaded.win_totem_highlight_02.texture,
        loaded.win_totem_highlight_03.texture,
        loaded.win_totem_highlight_04.texture,
        loaded.win_totem_highlight_05.texture,
        loaded.win_totem_highlight_06.texture,
        loaded.win_totem_highlight_07.texture,
        loaded.win_totem_highlight_08.texture,
        loaded.win_totem_highlight_09.texture,
        loaded.win_totem_highlight_10.texture,
        loaded.win_totem_highlight_11.texture,
        loaded.win_totem_highlight_12.texture,
        loaded.win_totem_highlight_13.texture,
        loaded.win_totem_highlight_14.texture,
        loaded.win_totem_highlight_15.texture,
    ];

    plateSprite = [
        loaded.plate01.texture,
        loaded.plate02.texture,
        loaded.plate03.texture,
        loaded.plate04.texture,
        loaded.plate05.texture,
    ];

    paytablePage = [
        loaded.paytable.texture,
        loaded.paylines.texture,
    ];

    cacheDynamicFonts();

    particles = [
        loaded.particle_a1.texture,
        loaded.particle_a2.texture,
        loaded.particle_a3.texture,
        loaded.particle_a4.texture,
        loaded.particle_a5.texture,
        loaded.particle_a6.texture,
        loaded.particle_a7.texture,
        loaded.particle_a8.texture
    ];

    background = new PIXI.Sprite(loader.resources.background.texture);
    foreground = new PIXI.Sprite(loader.resources.foreground.texture);
    plate1 = new PIXI.Sprite(plateSprite[4]);
    plate2 = new PIXI.Sprite(plateSprite[4]);
    plate3 = new PIXI.Sprite(plateSprite[4]);
    plate4 = new PIXI.Sprite(plateSprite[4]);
    plate1.name = 'plate1';
    plate1.x = isMobile ? 35 : 920;
    plate1.y = isMobile ? 775 : 370;
    plate1.scale.set(isMobile ? 1 : 0.9);
    plate2.x = isMobile ? 520 : 1125;
    plate2.y = isMobile ? 775 : 305;
    plate2.scale.set(isMobile ? 1 : 0.9);
    plate3.x = isMobile ? 100 : 965;
    plate3.y = isMobile ? 655 : 265;
    plate3.scale.set(isMobile ? 1 : 0.9);
    plate4.x = isMobile ? 445 : 1060;
    plate4.y = isMobile ? 630 : 205;
    plate4.scale.set(isMobile ? 1 : 0.9);
    plate1.alpha = 0;
    plate2.alpha = 0;
    plate3.alpha = 0;
    plate4.alpha = 0;

    table = new PIXI.Sprite(loader.resources.table.texture);

    characterSpine = new PIXI.spine.Spine(loader.resources.characterJson.spineData);
    characterSpine.autoUpdate = true;
    characterSpine.x = isMobile ? 370 : 1110;
    characterSpine.y = isMobile ? 780 : 430;
    characterSpine.scale.set(isMobile ? 2.5 : 1.8);

    bonusSpine = new PIXI.spine.Spine(loader.resources.bonus_splashJson.spineData);
    bonusSpine.autoUpdate = true;
    bonusSpine.x = isMobile ? 370 : app.getResolutionX() / 2;
    bonusSpine.y = isMobile ? 780 : app.getResolutionY() / 2;
    bonusSpine.scale.set(isMobile ? 1 : 1.5);
    bonusSpine.alpha = 0;

    playCharacterAnimation(characterSpine, characterAnimState.idleAnimation);

    characterSpine.state.addListener({
        complete: function (event) {
            playCharacterAnimation(characterSpine, characterAnimState.idleAnimation);
        },
    });

    app.stage.addChild(background);
    initiateSlots(totems);

    app.stage.addChild(foreground);
    app.stage.addChild(plate4);
    app.stage.addChild(plate3);
    app.stage.addChild(plate2);
    app.stage.addChild(plate1);
    app.stage.addChild(characterSpine);
    app.stage.addChild(table);
    app.stage.addChild(bonusSpine);

    // Paytable Components
    // Paytable sheet as button
    paytable = new PIXI.Sprite(paytablePage[0]);
    paytable.interactive = false;
    paytable.alpha = 0;

    button_close = new PIXI.Sprite(loader.resources.button_close.texture);
    button_close.interactive = false;
    button_close.x = isMobile ? 650 : 1200;
    button_close.y = isMobile ? 20 : 20;
    button_close.alpha = 0;
    button_close.on(isMobile ? 'pointertap' : 'click', closePayTable);

    button_previous = new PIXI.Sprite(loader.resources.button_next_previous.texture);
    button_previous.interactive = false;
    button_previous.anchor.set(0.5);
    button_previous.rotation = 3.15;
    button_previous.x = 29;
    button_previous.y = app.screen.height / 2;
    button_previous.alpha = 0;
    button_previous.on(isMobile ? 'pointertap' : 'click', switchPayTable);

    button_next = new PIXI.Sprite(loader.resources.button_next_previous.texture);
    button_next.interactive = false;
    button_next.anchor.set(0.5);
    button_next.x = app.screen.width - 29;
    button_next.y = app.screen.height / 2;
    button_next.alpha = 0;
    button_next.on(isMobile ? 'pointertap' : 'click', switchPayTable);

    // init panel
    uiPanel = panel.init({
        spinFn: gameSpin,
        betLineOptions: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
        spinAreaConfigX: app.isMobile() ? 360 : 0,
        spinAreaConfigY: app.isMobile() ? 535 : 0,
        spinAreaConfigWidth: app.isMobile() ? 720 : 0,
        spinAreaConfigHeight: app.isMobile() ? 600 : 0,
        betLinesUsage: true,
    });

    uiPanel.on('spinButtonClicked', () => {
        for (let i = 0; i < 15; i++) {
            dismissBetLines(i);
        }
    });

    uiPanel.on('betLineChanged', (betLine) => {
        showBetLines(betLine - 1);
    });

    uiPanel.on('betMaxButtonClicked', () => {
        showAllBetLines();
    });

    uiPanel.on('infoButtonClicked', () => {
        showPayTable();
    });

    uiPanel.on('turboButtonClicked', (speedUp) => {
        speed = speedUp ? 3 : 1;

        slots[0].setAccelerationSpeed(speed);
        slots[1].setAccelerationSpeed(speed);
        slots[2].setAccelerationSpeed(speed);
        slots[3].setAccelerationSpeed(speed);
        slots[4].setAccelerationSpeed(speed);
    })

    apiHelper.getStartBalance().then(function (balance) {

        // make sure balance is not undefined
        balance = balance ? balance : 0;

        state.set('balance', balance);
        panel.updateText();
        uiPanel.enableActionButtons();
    });
    //
    // ui.bringToFront(bonusSplatSpine);
    // ui.bringToFront(sprite_bonus_title);
    //
    // runRandomSound();

    app.stage.addChild(paytable);
    app.stage.addChild(button_close);
    app.stage.addChild(button_previous);
    app.stage.addChild(button_next);
}

export function initSound() {
    // sound.addMusic('bgSound', loaded.bgSound.sound, {loop: true, volume: 1});
    // sound.addMusic('sound_win_bonus_1', loaded.sound_win_bonus_1.sound, {loop: false, volume: 1});
    // sound.addMusic('sound_win_bonus_2', loaded.sound_win_bonus_2.sound, {loop: false, volume: 1});
    //
    // sound.addMusic('idle0', loaded.idle0Sound.sound, {loop: false, volume: 1});
    // sound.addMusic('idle1', loaded.idle1Sound.sound, {loop: false, volume: 1});
    // sound.addMusic('idle2', loaded.idle2Sound.sound, {loop: false, volume: 1});
    // sound.addMusic('idle3', loaded.idle3Sound.sound, {loop: false, volume: 1});
    // sound.addMusic('idle4', loaded.idle4Sound.sound, {loop: false, volume: 1});
    // sound.addMusic('stageIdle1', loaded.idleStage1Sound.sound, {loop: false, volume: 1});
    // sound.addMusic('stageIdle2', loaded.idleStage2Sound.sound, {loop: false, volume: 1});
    // sound.addMusic('stageIdle3', loaded.idleStage3Sound.sound, {loop: false, volume: 1});
    // sound.addMusic('spinSound', loaded.spinSound.sound, {loop: false, volume: 1});
    //
    sound.addSound('slotSpinningSound', loaded.slotSpinningSound.sound, {loop: true, volume: 1});
    sound.addSound('slotSpinningStopSound', loaded.slotSpinningStopSound.sound, {loop: false, volume: 1});
    sound.addSound('smallWinSound', loaded.smallWinSound.sound, {loop: false, volume: 1});
    sound.addSound('bigWinSound', loaded.bigWinSound.sound, {loop: false, volume: 1});
    sound.addSound('winMoneySound', loaded.moneySound.sound, {loop: false, volume: 1});

    // sound.play('bgSound');
}

// function runRandomSound() {
//     setTimeout(() => {
//         let tempRandomSound = Math.floor(Math.random() * 5)
//         if (tempRandomSound === 0)
//             sound.play('idle0');
//         else if (tempRandomSound === 1)
//             sound.play('idle1');
//         else if (tempRandomSound === 2)
//             sound.play('idle2');
//         else if (tempRandomSound === 3)
//             sound.play('idle3');
//         else if (tempRandomSound === 4)
//             sound.play('idle4');
//         runRandomSound();
//     }, 6000);
// }

function cacheDynamicFonts() {
    spriteNumberTexture = {
        '0': loaded.tex_num_zero.texture,
        '1': loaded.tex_num_one.texture,
        '2': loaded.tex_num_two.texture,
        '3': loaded.tex_num_three.texture,
        '4': loaded.tex_num_four.texture,
        '5': loaded.tex_num_five.texture,
        '6': loaded.tex_num_six.texture,
        '7': loaded.tex_num_seven.texture,
        '8': loaded.tex_num_eight.texture,
        '9': loaded.tex_num_nine.texture,
        '$': loaded.tex_num_dollar_sign.texture,
        '.': loaded.tex_num_dot.texture,
        ',': loaded.tex_num_comma.texture,
    };
}

function initiateSlots(totems) {

    slots = [];
    for (let i = 0; i < 5; i++) {
        let slot = new Slot({
            rows: 3,
            offset: isMobile ? 200 : 200,
            x: isMobile ? 105 + (i * 126) : 128 + (i * 174),
            y: isMobile ? 370 : 365,
            animationSpeed: 0.30,
            scale: isMobile ? 0.615 : 0.85,
        }, totems);

        slots.push(slot);

        // Add win panel
        const winPanelContainer = new PIXI.Container();
        winPanelContainer.name = `Win Panel Container ${i + 1}`;
        each(slot.getContainer().children, (singleSlot) => {
            winPanel = new PIXI.extras.AnimatedSprite(totemHighlight);
            const position = singleSlot.getGlobalPosition();
            winPanel.x = position.x;
            winPanel.y = position.y;
            winPanel.anchor.set(0.5);
            winPanel.alpha = 0;
            winPanel.animationSpeed = 0.33;
            winPanel.loop = false
            // winPanel.gotoAndPlay(15 - i);
            winPanelContainer.addChild(winPanel);
        });
        winPanels.push(winPanelContainer);
    }

    for (let i = 0; i < 5; i++) {
        app.addComponent(winPanels[i]);
        app.addComponent(slots[i].getContainer());
    }

    betLinesContainer = new PIXI.Container();
    each(patterns, (pattern, index) => {
        const array = pattern.path;
        betLine = ui.drawLine(codeToPosition(array), lineColourArray[index]);
        betLine.alpha = 0;
        betLinesContainer.addChild(betLine);
    });
    app.stage.addChild(betLinesContainer);
}

function codeToPosition(path) {

    if (typeof path === 'string') {
        path = path.split(', ');
    }

    return map(path, (code) => {
        const rowAndColumn = getRowAndColumn(code);
        return slots[rowAndColumn.column].slotContainer.children[rowAndColumn.row].getGlobalPosition();
    });
}

function getRowAndColumn(code) {
    let humanLayout = [
        ['a1', 'a2', 'a3', 'a4', 'a5'],
        ['b1', 'b2', 'b3', 'b4', 'b5'],
        ['c1', 'c2', 'c3', 'c4', 'c5']
    ];

    let column, row;
    for (let i = 0; i < humanLayout.length; i++) {
        for (let j = 0; j < humanLayout[i].length; j++) {
            if (code === humanLayout[i][j]) {
                column = j;
                row = i + 1;
                break;
            }
        }
    }

    return {row, column};
}

function showPayTable() {
    paytable.alpha = 1;
    paytable.texture = paytablePage[0];
    paytable.interactive = true;
    button_close.alpha = 1;
    button_close.buttonMode = true;
    button_close.interactive = true;
    button_previous.alpha = 1;
    button_previous.buttonMode = true;
    button_previous.interactive = true;
    button_next.alpha = 1;
    button_next.buttonMode = true;
    button_next.interactive = true;
}

function closePayTable() {
    paytable.alpha = 0;
    paytable.interactive = false;
    button_close.alpha = 0;
    button_close.buttonMode = false;
    button_close.interactive = false;
    button_previous.alpha = 0;
    button_previous.buttonMode = false;
    button_previous.interactive = false;
    button_next.alpha = 0;
    button_next.buttonMode = false;
    button_next.interactive = false;
}

function switchPayTable() {
    if (paytable.texture === paytablePage[1])
        paytable.texture = paytablePage[0];
    else if (paytable.texture === paytablePage[0])
        paytable.texture = paytablePage[1];
}

function gameSpin(isAutoPlay = false) {

    slots[0].startSpin();
    TweenMax.delayedCall(0.15 / speed, function () {
        slots[1].startSpin();
    });
    TweenMax.delayedCall(0.35 / speed, function () {
        slots[2].startSpin();
    });
    TweenMax.delayedCall(0.55 / speed, function () {
        slots[3].startSpin();
    });
    TweenMax.delayedCall(0.75 / speed, function () {
        slots[4].startSpin();
    });
    sound.play('slotSpinningSound');

    // wait at least 2.5 seconds only stop the slots to prevent API return too fast
    return Promise.all([
        apiHelper.getSpinData(state.get('betAmount'), state.get('betLine'), isAutoPlay),
        new Promise((resolve) => {
            setTimeout(resolve, 2500 / speed);
        })
    ]).then(([data]) => {
        let newData = [];
        for (let i = 0; i < 5; i++) {
            newData[i] = [];
            newData[i].push(data.slots[0][i]);
            newData[i].push(data.slots[1][i]);
            newData[i].push(data.slots[2][i]);
        }

        slots[0].stopSpin(newData[0]);
        setTimeout(() => {
            slots[1].stopSpin(newData[1]);
        }, 400 / speed);
        setTimeout(() => {
            slots[2].stopSpin(newData[2]);
        }, 800 / speed);
        setTimeout(() => {
            slots[3].stopSpin(newData[3]);
        }, 1200 / speed);
        setTimeout(() => {
            slots[4].stopSpin(newData[4]);
        }, 1600 / speed);
        sound.stop('slotSpinningSound');
        sound.play('slotSpinningStopSound');

        // delay 1 second to make sure all animation stop.
        return (new Promise((resolve) => {
            setTimeout(() => {
                bonusChargeLevel = data.bonusLevel;
                checkWin(data, data.bonus, data.bonusLevel, data.scatters, data.matches).then((checkWinObj) => {
                    resolve(checkWinObj);
                });
            }, 2000 / speed);
        }));
    });
}

function checkWin(data, bonus, bonusLevel, scatterDetails, matches) {

    if (data.payout === 0 && (!scatterDetails || scatterDetails.scatter_bonus === 0)) {
        return Promise.resolve(data.payout);
    }

    let bonusToken = undefined;
    let bonusValue = undefined;
    if (bonus) {
        bonusToken = data.bonus.token;
        bonusValue = data.bonus.value;
    }

    if (bonusLevel === 0 && !data.hasOwnProperty('bonus')) {
        plate1.alpha = 0;
        plate2.alpha = 0;
        plate3.alpha = 0;
        plate4.alpha = 0;
    }
    else if (bonusLevel >= 1 && bonusLevel <= 5) {
        plate1.alpha = 1;
        plate2.alpha = 0;
        plate3.alpha = 0;
        plate4.alpha = 0;
        for (let i = 1; i <= 5; i++) {
            if (bonusLevel === i)
                plate1.texture = plateSprite[i - 1];
        }
    }
    else if (bonusLevel >= 6 && bonusLevel <= 10) {
        plate1.alpha = 1;
        plate2.alpha = 1;
        plate3.alpha = 0;
        plate4.alpha = 0;
        plate1.texture = plateSprite[4];
        for (let i = 1; i <= 5; i++) {
            if (bonusLevel === (5 + i))
                plate2.texture = plateSprite[i - 1];
        }
    }
    else if (bonusLevel >= 11 && bonusLevel <= 15) {
        plate1.alpha = 1;
        plate2.alpha = 1;
        plate3.alpha = 1;
        plate4.alpha = 0;
        plate1.texture = plateSprite[4];
        plate2.texture = plateSprite[4];
        for (let i = 1; i <= 5; i++) {
            if (bonusLevel === (10 + i))
                plate3.texture = plateSprite[i - 1];
        }
    }
    else if (bonusLevel >= 16 && bonusLevel <= 19) {
        plate1.alpha = 1;
        plate2.alpha = 1;
        plate3.alpha = 1;
        plate4.alpha = 1;
        plate1.texture = plateSprite[4];
        plate2.texture = plateSprite[4];
        plate3.texture = plateSprite[4];
        for (let i = 1; i <= 4; i++) {
            if (bonusLevel === (15 + i))
                plate4.texture = plateSprite[i - 1];
        }
    }
    else if (bonusLevel === 0 && data.hasOwnProperty('bonus')) {
        plate1.alpha = 1;
        plate2.alpha = 1;
        plate3.alpha = 1;
        plate4.alpha = 1;
        plate1.texture = plateSprite[4];
        plate2.texture = plateSprite[4];
        plate3.texture = plateSprite[4];
        plate4.texture = plateSprite[4];
    }

    playCharacterAnimation(characterSpine, characterAnimState.winAnimation);

    let scatterPromise = new Promise((resolve) => {
        // Return empty resolve if no scatter details
        if (!scatterDetails) {
            return resolve();
        }

        // Return empty resolve if got scatter details, but count <= 1
        if (scatterDetails.count <= 1) {
            return resolve();
        }

        const scatterPath = scatterDetails.path.split(', ');
        let scatterPositions = map(scatterPath, (path, index) => {
            return getRowAndColumn(path);
        });
        each(scatterPositions, (position, index) => {
            slots[position.column].playAnimationAtIndex(position.row);
        });

        uiPanel.winFlash(scatterDetails.scatter_bonus);
        let winAmountSprite = panel.displayWinAmountWithSprites({
            xPosition: isMobile ? 362 : 460,
            yPosition: isMobile ? 550 : 450,
            tweenToY: -300,
            spritedNumbersOffset: isMobile ? 80 : 105,
            spritedNumbersScaleX: isMobile ? 0.75 : 1.05,
            spritedNumbersScaleY: isMobile ? 0.75 : 1.05,
        }, 3, scatterDetails.scatter_bonus, spriteNumberTexture, particles);
        app.addComponent(winAmountSprite);

        if (isMobile)
            ui.bringBehindDrawer(winAmountSprite);

        sound.play('smallWinSound');

        showMatchedBetLine(scatterPath).then(() => {
            // Wait 1200ms for all animation to stop
            setTimeout(() => {
                each(scatterPositions, (position, index) => {
                    slots[position.column].stopAnimationAtIndex(position.row, 0);
                });

                resolve(scatterDetails.scatter_bonus);
            }, 1200 / speed);
        });
    });

    function runBonusAnimation() {
        return new Promise((resolve) => {
            if (!data.hasOwnProperty('bonus')) {
                return resolve({});
            }

            setTimeout(() => {
            bonusSpine.state.setAnimation(0, 'bonus_splash', false);
                plate1.alpha = 1;
                plate2.alpha = 1;
                plate3.alpha = 1;
                plate4.alpha = 1;
                plate1.texture = plateSprite[4];
                plate2.texture = plateSprite[4];
                plate3.texture = plateSprite[4];
                plate4.texture = plateSprite[4];

                bonusSpine.alpha = 1;

                setTimeout(() => {
                    const runningNumberContainerOptions = {
                        offset: app.isMobile() ? 90 : 120,
                        scaleX: app.isMobile() ? 0.55 : 1,
                        scaleY: app.isMobile() ? 0.55 : 1,
                    };
                    let runningNumber = {
                        val: 0,
                        dummyNumber: 0
                    };

                    let runningNumberContainer = new PIXI.Container();
                    TweenMax.to(runningNumber, 4.5, {
                        dummyNumber: 200,
                        // Starts bonus running numbers
                        onStart: () => {
                            // bonusSplatSpine.alpha = 1;
                            // sprite_bonus_title.alpha = 1;
                            // sprite_bonus_title.y = 135;
                            runningNumberContainer.y = app.getResolutionY() / 2;

                            TweenMax.to(runningNumber, 2.5, {
                                val: bonusValue,
                                ease: Power4.easeInOut,
                                onUpdate: function () {
                                    if (runningNumberContainer)
                                        app.stage.removeChild(runningNumberContainer);

                                    let updatedString = utils.showAsCurrency(runningNumber.val);
                                    updatedString = '$' + updatedString.toString();

                                    runningNumberContainer = ui.createSpritedNumbers(runningNumberContainerOptions, spriteNumberTexture, updatedString);
                                    runningNumberContainer.x = app.getResolutionX() / 2 - 10 - runningNumberContainer.getBounds().width / 2;
                                    runningNumberContainer.y = runningNumberContainer.y = app.getResolutionY() / 2 - 200;
                                    app.addComponent(runningNumberContainer);
                                },
                            });
                        },

                        // Updates y value of bonus title and running numbers based on spine.y
                        onUpdate: () => {
                            if (!runningNumberContainer)
                                return;

                            // let newY = bonusSplatSpine.children[0].children[0].getBounds().y + (bonusSplatSpine.children[0].children[0].getBounds().height / 2);
                            // runningNumberContainer.y = newY + 46;
                            // sprite_bonus_title.y = newY - 132;
                        },

                        onComplete: function () {
                            // TweenMax.to(sprite_bonus_background, 1, {alpha: 0});
                            runningNumberContainer.destroy();

                            apiHelper.postBonusValue(bonusToken, bonusValue);
                            uiPanel.winFlash(bonusValue);
                            plate1.alpha = 0;
                            plate2.alpha = 0;
                            plate3.alpha = 0;
                            plate4.alpha = 0;
                            bonusSpine.alpha = 0;
                            return resolve({
                                isBonus: true,
                                bonusValue: bonusValue,
                            });
                        },
                    });
                }, 2000);
            }, 500 * speed);

            // Running Numbers

        });
    }

    return scatterPromise.then((scatterPayout) => {

        let checkPromises = map(matches, (match, index) => {
            return new Promise((resolve) => {
                TweenMax.delayedCall(index / speed, () => {
                    //     y: isMobile ? 530 : 310
                    // flash the wallet
                    uiPanel.winFlash(match.winAmount);
                    let winAmountSprite = panel.displayWinAmountWithSprites({
                        xPosition: isMobile ? 362 : 460,
                        yPosition: isMobile ? 550 : 450,
                        tweenToY: -300,
                        spritedNumbersOffset: isMobile ? 80 : 105,
                        spritedNumbersScaleX: isMobile ? 0.75 : 1.05,
                        spritedNumbersScaleY: isMobile ? 0.75 : 1.05,
                    }, 3, match.winAmount, spriteNumberTexture, particles);
                    app.addComponent(winAmountSprite);
                    if (isMobile)
                        ui.bringBehindDrawer(winAmountSprite);
                    // sound.play('smallWinSound');
                    if (data.payout / state.get('betAmount') >= 100) {
                        sound.play('bigWinSound');
                    }
                    else
                        sound.play('smallWinSound');

                    let codes = match.path.split(', ');

                    let matchRowCol = map(codes, (path, index) => {
                        return getRowAndColumn(path);
                    });
                    each(matchRowCol, (position, index) => {
                        slots[position.column].playAnimationAtIndex(position.row);
                    });

                    showMatchedBetLine(codes).then(() => {
                        setTimeout(() => {
                            // each(matchRowCol, (position, index) => {
                            //     slots[position.column].stopAnimationAtIndex(position.row, 0);
                            // });
                            resolve(match.winAmount);
                        }, 1200 / speed);
                    });
                });
            });
        });

        return Promise.all(checkPromises).then((payout) => {
            return new Promise((resolve) => {
                let totalPayout = sum(payout);
                totalPayout += scatterPayout;

                let bonusResolve = runBonusAnimation();
                bonusResolve.then((bonusObject) => {
                    const checkWinObject = Object.assign({
                        payout: totalPayout,
                        isBonus: false,
                        bonusValue: 0,
                    }, bonusObject);
                    resolve(checkWinObject);
                });
            });
        })
    });
}

function showMatchedBetLine(codes) {
    return Promise.all(map(codes, (code) => {
        const rowAndColumn = getRowAndColumn(code);
        winPanels[0].getChildAt(rowAndColumn.row).gotoAndPlay(0);
        setTimeout(() => {
            winPanels[1].getChildAt(rowAndColumn.row).gotoAndPlay(0);
            setTimeout(() => {
                winPanels[2].getChildAt(rowAndColumn.row).gotoAndPlay(0);
                setTimeout(() => {
                    winPanels[3].getChildAt(rowAndColumn.row).gotoAndPlay(0);
                    setTimeout(() => {
                        winPanels[4].getChildAt(rowAndColumn.row).gotoAndPlay(0);
                    }, 40 / speed);
                }, 40 / speed);
            }, 40 / speed);
        }, 40 / speed);
        return new Promise((resolve) => {
            TweenMax.to(winPanels[rowAndColumn.column].getChildAt(rowAndColumn.row), 0.4 / speed, {
                alpha: 1,
                repeat: 1,
                yoyo: true,
                ease: Power2.easeOut,
                onComplete: resolve
            });
        })
    }));
}

function showBetLines(betLineIndex) {
    TweenMax.to(betLinesContainer.getChildAt(betLineIndex), 1 / speed, {
        alpha: 1,
        repeat: 1,
        yoyo: true,
        ease: Power4.easeOut,
    });

    // to make sure the bet line will dismiss at the end
    setTimeout(() => {
        dismissBetLines(betLineIndex)
    }, 2000 / speed);
}

function showAllBetLines() {
    for (let i = 0; i < 15; i++) {
        TweenMax.to(betLinesContainer.getChildAt(i), 1 / speed, {
            alpha: 1,
            repeat: 1,
            yoyo: true,
            ease: Power4.easeOut,
        });
    }

    // to make sure the bet line will dismiss at the end
    setTimeout(() => {
        for (let i = 0; i < 15; i++) {
            TweenMax.to(betLinesContainer.getChildAt(i), 0.5 / speed, {alpha: 0});
        }
    }, 2000 / speed);
}

function dismissBetLines(betLineIndex) {
    TweenMax.to(betLinesContainer.getChildAt(betLineIndex), 0.5 / speed, {alpha: 0});
}

function playCharacterAnimation(characterSpine, animState) {
    switch (animState) {
        case (characterAnimState.idleAnimation):
            let characterIdleRandom = (Math.floor(Math.random() * 3));
            if (characterIdleRandom === 0)
                characterSpine.state.setAnimation(0, 'idel00', false);
            else if (characterIdleRandom === 1)
                characterSpine.state.setAnimation(0, 'idel01', false);
            else if (characterIdleRandom === 2)
                characterSpine.state.setAnimation(0, 'idel02', false);
            break;
        case characterAnimState.winAnimation:
            characterSpine.state.setAnimation(0, 'win', 3);
            break;
    }
}

export default {
    init,
    initSound
}
