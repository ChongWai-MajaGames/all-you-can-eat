import apiService from './core/api';

function getStartBalance() {
    return apiService.post('/auth/member/me').then((data) => {
        return data.balance;
    });
}

function getSpinData(betAmount, betLine, isAutoplay) {
    let data = {bet_amount: betAmount, bet_line: betLine, isAutoPlay: isAutoplay};
    return apiService.post('/play/feed-the-zombie', data);
}

function postBonusValue(token, value) {
    let data = {bonusToken: token, value: value};
    return apiService.post('/bonus', data);
}

export default {
    getStartBalance,
    getSpinData,
    postBonusValue,
}
