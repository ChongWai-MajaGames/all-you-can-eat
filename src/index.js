import app from './core/app';
import game from './game';
import utils from "./core/utils";

PIXI.utils.sayHello('Gamegeon: All You Can Eat');

let isMobile = app.isMobile();
let loader = app.getLoader();
let lang = utils.getQueryParam('lang') || 'en';

if (isMobile) {
    loader
    // totems
        .add('totem_a', './assets/totems/totem_a.png')
        .add('totem_b', './assets/totems/totem_b.png')
        .add('totem_c', './assets/totems/totem_c.png')
        .add('totem_d', './assets/totems/totem_d.png')
        .add('totem_e', './assets/totems/totem_e.png')
        .add('totem_f', './assets/totems/totem_f.png')
        .add('totem_g', './assets/totems/totem_g.png')
        .add('totem_h', './assets/totems/totem_h.png')
        .add('totem_x', './assets/totems/totem_x.png')
        .add('totem_y', './assets/totems/totem_y.png')

        .add('texture_scatter_1', './assets/totems/scatter_animation_01.png')
        .add('texture_scatter_2', './assets/totems/scatter_animation_02.png')
        .add('texture_scatter_3', './assets/totems/scatter_animation_03.png')
        .add('texture_scatter_4', './assets/totems/scatter_animation_04.png')
        .add('texture_scatter_5', './assets/totems/scatter_animation_05.png')
        .add('texture_scatter_6', './assets/totems/scatter_animation_06.png')
        .add('texture_scatter_7', './assets/totems/scatter_animation_07.png')
        .add('texture_scatter_8', './assets/totems/scatter_animation_08.png')
        .add('texture_scatter_9', './assets/totems/scatter_animation_09.png')
        .add('texture_scatter_10', './assets/totems/scatter_animation_10.png')
        .add('texture_scatter_11', './assets/totems/scatter_animation_11.png')
        .add('texture_scatter_12', './assets/totems/scatter_animation_12.png')
        .add('texture_scatter_13', './assets/totems/scatter_animation_13.png')
        .add('texture_scatter_14', './assets/totems/scatter_animation_14.png')

        .add('texture_wildcard_1', './assets/totems/wild_animation_01.png')
        .add('texture_wildcard_2', './assets/totems/wild_animation_02.png')
        .add('texture_wildcard_3', './assets/totems/wild_animation_03.png')
        .add('texture_wildcard_4', './assets/totems/wild_animation_04.png')
        .add('texture_wildcard_5', './assets/totems/wild_animation_05.png')
        .add('texture_wildcard_6', './assets/totems/wild_animation_06.png')
        .add('texture_wildcard_7', './assets/totems/wild_animation_07.png')
        .add('texture_wildcard_8', './assets/totems/wild_animation_08.png')
        .add('texture_wildcard_9', './assets/totems/wild_animation_09.png')
        .add('texture_wildcard_10', './assets/totems/wild_animation_10.png')
        .add('texture_wildcard_11', './assets/totems/wild_animation_11.png')
        .add('texture_wildcard_12', './assets/totems/wild_animation_12.png')
        .add('texture_wildcard_13', './assets/totems/wild_animation_13.png')
        .add('texture_wildcard_14', './assets/totems/wild_animation_14.png')
        .add('texture_wildcard_15', './assets/totems/wild_animation_15.png')
        .add('texture_wildcard_16', './assets/totems/wild_animation_16.png')
        .add('texture_wildcard_17', './assets/totems/wild_animation_17.png')
        .add('texture_wildcard_18', './assets/totems/wild_animation_18.png')
        .add('texture_wildcard_19', './assets/totems/wild_animation_19.png')

        .add('win_totem_highlight_01', './assets/win_totem_highlight_01.png')
        .add('win_totem_highlight_02', './assets/win_totem_highlight_02.png')
        .add('win_totem_highlight_03', './assets/win_totem_highlight_03.png')
        .add('win_totem_highlight_04', './assets/win_totem_highlight_04.png')
        .add('win_totem_highlight_05', './assets/win_totem_highlight_05.png')
        .add('win_totem_highlight_06', './assets/win_totem_highlight_06.png')
        .add('win_totem_highlight_07', './assets/win_totem_highlight_07.png')
        .add('win_totem_highlight_08', './assets/win_totem_highlight_08.png')
        .add('win_totem_highlight_09', './assets/win_totem_highlight_09.png')
        .add('win_totem_highlight_10', './assets/win_totem_highlight_10.png')
        .add('win_totem_highlight_11', './assets/win_totem_highlight_11.png')
        .add('win_totem_highlight_12', './assets/win_totem_highlight_12.png')
        .add('win_totem_highlight_13', './assets/win_totem_highlight_13.png')
        .add('win_totem_highlight_14', './assets/win_totem_highlight_14.png')
        .add('win_totem_highlight_15', './assets/win_totem_highlight_15.png')

        .add('button_close', './assets/button_close.png')
        .add('button_next_previous', './assets/button_next_previous.png')
        .add('plate01', './assets/plate01.png')
        .add('plate02', './assets/plate02.png')
        .add('plate03', './assets/plate03.png')
        .add('plate04', './assets/plate04.png')
        .add('plate05', './assets/plate05.png')
        .add('table', './assets/table_p.png')
        .add('characterJson', './assets/spine/Jimmy.json')
        .add('characterAtlas', './assets/spine/Jimmy.atlas')
        .add('bonus_splashJson', './assets/spine/bonu_splash.json')
        .add('bonus_splashAtlas', './assets/spine/bonu_splash.atlas')

    // language
        .add('background', `./assets/languages/${lang}/background_p.png`)
        .add('foreground', `./assets/languages/${lang}/foreground_p.png`)
        .add('paylines', `./assets/languages/${lang}/paylines_p.png`)
        .add('paytable', `./assets/languages/${lang}/paytable_p.png`)

} else {
    loader
    // totems
        .add('totem_a', './assets/totems/totem_a.png')
        .add('totem_b', './assets/totems/totem_b.png')
        .add('totem_c', './assets/totems/totem_c.png')
        .add('totem_d', './assets/totems/totem_d.png')
        .add('totem_e', './assets/totems/totem_e.png')
        .add('totem_f', './assets/totems/totem_f.png')
        .add('totem_g', './assets/totems/totem_g.png')
        .add('totem_h', './assets/totems/totem_h.png')
        .add('totem_x', './assets/totems/totem_x.png')
        .add('totem_y', './assets/totems/totem_y.png')

        .add('texture_scatter_1', './assets/totems/scatter_animation_01.png')
        .add('texture_scatter_2', './assets/totems/scatter_animation_02.png')
        .add('texture_scatter_3', './assets/totems/scatter_animation_03.png')
        .add('texture_scatter_4', './assets/totems/scatter_animation_04.png')
        .add('texture_scatter_5', './assets/totems/scatter_animation_05.png')
        .add('texture_scatter_6', './assets/totems/scatter_animation_06.png')
        .add('texture_scatter_7', './assets/totems/scatter_animation_07.png')
        .add('texture_scatter_8', './assets/totems/scatter_animation_08.png')
        .add('texture_scatter_9', './assets/totems/scatter_animation_09.png')
        .add('texture_scatter_10', './assets/totems/scatter_animation_10.png')
        .add('texture_scatter_11', './assets/totems/scatter_animation_11.png')
        .add('texture_scatter_12', './assets/totems/scatter_animation_12.png')
        .add('texture_scatter_13', './assets/totems/scatter_animation_13.png')
        .add('texture_scatter_14', './assets/totems/scatter_animation_14.png')

        .add('texture_wildcard_1', './assets/totems/wild_animation_01.png')
        .add('texture_wildcard_2', './assets/totems/wild_animation_02.png')
        .add('texture_wildcard_3', './assets/totems/wild_animation_03.png')
        .add('texture_wildcard_4', './assets/totems/wild_animation_04.png')
        .add('texture_wildcard_5', './assets/totems/wild_animation_05.png')
        .add('texture_wildcard_6', './assets/totems/wild_animation_06.png')
        .add('texture_wildcard_7', './assets/totems/wild_animation_07.png')
        .add('texture_wildcard_8', './assets/totems/wild_animation_08.png')
        .add('texture_wildcard_9', './assets/totems/wild_animation_09.png')
        .add('texture_wildcard_10', './assets/totems/wild_animation_10.png')
        .add('texture_wildcard_11', './assets/totems/wild_animation_11.png')
        .add('texture_wildcard_12', './assets/totems/wild_animation_12.png')
        .add('texture_wildcard_13', './assets/totems/wild_animation_13.png')
        .add('texture_wildcard_14', './assets/totems/wild_animation_14.png')
        .add('texture_wildcard_15', './assets/totems/wild_animation_15.png')
        .add('texture_wildcard_16', './assets/totems/wild_animation_16.png')
        .add('texture_wildcard_17', './assets/totems/wild_animation_17.png')
        .add('texture_wildcard_18', './assets/totems/wild_animation_18.png')
        .add('texture_wildcard_19', './assets/totems/wild_animation_19.png')

        .add('win_totem_highlight_01', './assets/win_totem_highlight_01.png')
        .add('win_totem_highlight_02', './assets/win_totem_highlight_02.png')
        .add('win_totem_highlight_03', './assets/win_totem_highlight_03.png')
        .add('win_totem_highlight_04', './assets/win_totem_highlight_04.png')
        .add('win_totem_highlight_05', './assets/win_totem_highlight_05.png')
        .add('win_totem_highlight_06', './assets/win_totem_highlight_06.png')
        .add('win_totem_highlight_07', './assets/win_totem_highlight_07.png')
        .add('win_totem_highlight_08', './assets/win_totem_highlight_08.png')
        .add('win_totem_highlight_09', './assets/win_totem_highlight_09.png')
        .add('win_totem_highlight_10', './assets/win_totem_highlight_10.png')
        .add('win_totem_highlight_11', './assets/win_totem_highlight_11.png')
        .add('win_totem_highlight_12', './assets/win_totem_highlight_12.png')
        .add('win_totem_highlight_13', './assets/win_totem_highlight_13.png')
        .add('win_totem_highlight_14', './assets/win_totem_highlight_14.png')
        .add('win_totem_highlight_15', './assets/win_totem_highlight_15.png')

        .add('button_close', './assets/button_close.png')
        .add('button_next_previous', './assets/button_next_previous.png')
        .add('plate01', './assets/plate01.png')
        .add('plate02', './assets/plate02.png')
        .add('plate03', './assets/plate03.png')
        .add('plate04', './assets/plate04.png')
        .add('plate05', './assets/plate05.png')
        .add('table', './assets/table_l.png')
        .add('characterJson', './assets/spine/Jimmy.json')
        .add('characterAtlas', './assets/spine/Jimmy.atlas')
        .add('bonus_splashJson', './assets/spine/bonu_splash.json')
        .add('bonus_splashAtlas', './assets/spine/bonu_splash.atlas')

        // language
        .add('background', `./assets/languages/${lang}/background_l.png`)
        .add('foreground', `./assets/languages/${lang}/foreground_l.png`)
        .add('paylines', `./assets/languages/${lang}/paylines_l.png`)
        .add('paytable', `./assets/languages/${lang}/paytable_l.png`)
}

// loader
//     .on('start', function () {
//         app.loadingStart({
//             loadingRectColor: 0x00ff00,
//         });
//     })

// start the assets and init game when load completed.
loader.on('complete', function () {
    game.init();
    game.initSound();
}).load();
